require 'lib.player'
require 'lib.bullet'
require 'lib.enemy'

debug = true

function move(table, dt)
    if table ~= nil then
        for i,v in ipairs(table) do
            local startX = v.x
            local startY = v.y
            local playerX = player.x
            local playerY = player.y

            local angle = math.atan2((playerY - startY), (playerX - startX))

            v.dx = enemy.speed * math.cos(angle)
            v.dy = enemy.speed * math.sin(angle)
            v.x = v.x + (v.dx * dt)
            v.y = v.y + (v.dy * dt)

        end
    end
end

function shoot(table, dt)
    if table ~= nil then
        for i,v in ipairs(table) do
            v.x = v.x + (v.dx * dt)
            v.y = v.y + (v.dy * dt)

        end
    end
end

function love.load()
    enemy:load()
    player:load()
    bullet:load()
end

function love.update(dt)
    shoot(activeBullets, dt)
    move(activeEnemy, dt)
    enemy:update(dt)
    player:update(dt)
    bullet:update(dt)
end

function love.draw()
    bullet:draw()
    enemy:draw()
    player:draw()
    player:debug(false)
end
