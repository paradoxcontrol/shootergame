enemy = { }


function enemy:make()
    if self.spawnTimer == self.spawn then
        local startX = math.random(0, 1024)
        local startY = math.random(0, 768)
        local playerX = player.x
        local playerY = player.y

        local angle = math.atan2((playerY - startY), (playerX - startX))

        local enemyDx = enemy.speed * math.cos(angle)
        local enemyDy = enemy.speed * math.sin(angle)

        table.insert(activeEnemy, {x = startX, y = startY, dx = enemyDx, dy = enemyDy})
        self.spawnTimer = 0
    else
        self.spawnTimer = self.spawnTimer + 1
    end
end

function enemy:create()
    if activeEnemy ~= nil then
        for i,v in ipairs(activeEnemy) do
            love.graphics.setColor(self.color.r, self.color.g, self.color.b)
            love.graphics.rectangle('fill', v.x, v.y, self.width, self.height)
        end
    end
end


function enemy:load()
--Put all your loading functions here
    enemy.hp = 10
    enemy.speed = 150
    enemy.x = 100
    enemy.y = 100
    enemy.height = 35
    enemy.width = 35
    enemy.spawnTimer = 50
    enemy.spawn = 50
    enemy.color = {r = 0, g = 255, b = 0}
    activeEnemy = { }
end

function enemy:update(dt)
--Put all your update stuff in here.
    enemy:make(dt)
end

function enemy:draw()
--Put all your draw stuff here.
    enemy:create()
end
