bullet = {}

function bullet:create()
    if activeBullets ~= nil then
        for i,v in ipairs(activeBullets) do
            love.graphics.setColor(self.color.r, self.color.g, self.color.b)
            love.graphics.rectangle('fill', v.x, v.y, self.width, self.height)
        end
    end
end

-- Main Loop Functions
function bullet:load()
-- Main Stats
    self.height = 5
    self.width = 5
    self.color = {r = 255, g = 255, b = 255}
    self.speed = 300
    self.damage = 2
end

function bullet:draw()
    bullet:create()
end

function bullet:update(dt)
end
