require 'lib.bullet'
player = { }

-- Aim function is used to animate the player to be facing the mouse.
function player:aim(x1,y1)
    local x2,y2 = love.mouse.getPosition()
    rotation = math.atan2(y2-y1, x2-x1)
    return rotation
end

function player:move(dt)
    if love.keyboard.isDown('w') then
        self.y = self.y - (self.speed * dt)
    end
    if love.keyboard.isDown('s') then
        self.y = self.y + (self.speed * dt)
    end
    if love.keyboard.isDown('a') then
        self.x = self.x - (self.speed * dt)
    end
    if love.keyboard.isDown('d') then
        self.x = self.x + (self.speed * dt)
    end
end

-- Attack Function. Creates bullets and fires them towards the mouse.
function player:shoot()
    if self.atktimer == self.atkspeed then
        if love.mouse.isDown('l') then
            local startX = self.x
            local startY = self.y
            local mouseX = love.mouse.getX()
            local mouseY = love.mouse.getY()

            local angle = math.atan2((mouseY - startY), (mouseX - startX))

            local bulletDx = bullet.speed * math.cos(angle)
            local bulletDy = bullet.speed * math.sin(angle)

            table.insert(activeBullets, {x = startX, y = startY, dx = bulletDx, dy = bulletDy})
            self.atktimer = 0
        end
    else
        self.atktimer = self.atktimer + 1
    end
end

---Main Player Loop Functions
function player:load()
-- Player Stats --
    bullet:load()
    activeBullets = {}
    self.img = love.graphics.newImage("assets/images/tank.png")
    self.hp = 100
    self.speed = 100
    self.x = 500
    self.y = 500
    self.width = self.img:getWidth()
    self.height = self.img:getHeight()
    self.color = {r = 255, g = 0, b = 255}
    self.atkspeed = 5
    self.atktimer = 5
end

function player:draw()
    love.graphics.setColor(255, 255, 255)
    rot = player:aim(self.x,self.y)
    love.graphics.draw(self.img,self.x,self.y,rot,1,1,self.width/2,self.height/2)
end

function player:debug(state)
    if state then
        love.graphics.print('Debug Log:', 0, 0)
        for i,v in ipairs(activeBullets) do
            position = i * 15
            love.graphics.print('Bullet ' .. i .. ' X: ' .. v.x .. ' Y: ' .. v.y, 0, 0 + position)
        end
    end
end

function player:update(dt)
    player:move(dt)
    player:shoot(dt)
end
